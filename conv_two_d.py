import os
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt

plt.switch_backend("agg")

fig_width = 16
fig_height = 9
frame_width = 4
frame_height = 4
kernel_scale = .4
img_spacing = .5
dpi = 100
tan = "#e1ddbf"
bg_color = "xkcd:bluey grey"
# bg_color = "#4c837a"
# bg_color = "xkcd:midnight"
# bg_color = "xkcd:off white"
border_color = "#04253a"
# border_color = "xkcd:off white"
# border_color = "midnightblue"
# text_color = "xkcd:off white"
text_color = "#04253a"
# text_color = "midnightblue"
text_size = 20
pos_neg_cmap = LinearSegmentedColormap.from_list(
    "pos_neg",
    [(0, "red"), (.5, "black"), (1, "white")])


def render(image, kernel, name="conv"):
    n_image_rows, n_image_cols = image.shape
    n_kernel_rows, n_kernel_cols = kernel.shape
    n_result_rows = n_image_rows - n_kernel_rows + 1
    n_result_cols = n_image_cols - n_kernel_cols + 1
    results = np.zeros((n_result_rows, n_result_cols))

    image_width = (
        frame_width * n_image_cols / np.maximum(n_image_rows, n_image_cols))
    image_height = (
        frame_height * n_image_rows / np.maximum(n_image_rows, n_image_cols))
    pixel_width = image_width / n_image_cols
    pixel_height = image_height / n_image_rows

    reversed_kernel = np.rot90(kernel, k=2, axes=(0, 1))
    for i_result_col in range(n_result_cols):
        for i_result_row in range(n_result_rows):
            patch = image[
                i_result_row: i_result_row + n_kernel_rows,
                i_result_col: i_result_col + n_kernel_cols]
            product = reversed_kernel * patch
            results[i_result_row, i_result_col] = np.sum(product)

    fig = plt.figure(figsize=(fig_width, fig_height))
    ax_fig = fig.add_axes((0, 0, 1, 1), facecolor=bg_color)
    blank(ax_fig)
    ax_fig.set_xlim(0, fig_width)
    ax_fig.set_ylim(0, fig_height)

    # Create the first figure with the kernel
    left = fig_width / 2 - 1.5 * frame_width - img_spacing
    bottom = (fig_height - frame_height) / 2
    center = left + frame_width / 2
    text_height = bottom + frame_height * 1.15
    ax_kernel_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    blank(ax_kernel_frame)
    ax_kernel_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "kernel")

    kernel_height = (
        n_kernel_rows /
        np.maximum(n_kernel_rows, n_kernel_cols) *
        frame_height *
        kernel_scale)
    kernel_width = (
        n_kernel_cols /
        np.maximum(n_kernel_rows, n_kernel_cols) *
        frame_width *
        kernel_scale)
    kernel_left = left + (frame_width - kernel_width) / 2
    kernel_bottom = bottom + (frame_height - kernel_height) / 2
    ax_kernel = fig.add_axes((
        kernel_left / fig_width,
        kernel_bottom / fig_height,
        kernel_width / fig_width,
        kernel_height / fig_height))
    scaled_kernel = kernel / np.max(np.abs(kernel) + 1e-6)
    ax_kernel.imshow(scaled_kernel, cmap=pos_neg_cmap, vmin=-1, vmax=1)
    blank(ax_kernel)

    # Create the second figure with the image
    left = fig_width / 2 - frame_width / 2
    center = left + frame_width / 2
    ax_img_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    blank(ax_img_frame)
    ax_img_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "image")

    image_left = left + (frame_width - image_width) / 2
    image_bottom = bottom +  (frame_height - image_height) / 2
    ax_img = fig.add_axes((
        image_left / fig_width,
        image_bottom / fig_height,
        image_width / fig_width,
        image_height / fig_height))
    scaled_image = image / np.max(np.abs(image) + 1e-6)
    ax_img.imshow(scaled_image, cmap=pos_neg_cmap, vmin=-1, vmax=1)
    blank(ax_img)

    # Create the third figure with the convolution results
    left = fig_width / 2 + frame_width / 2 + img_spacing
    center = left + frame_width / 2
    ax_result_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    blank(ax_result_frame)
    ax_result_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "convolution")

    result_height = n_result_rows * pixel_height
    result_width = n_result_cols * pixel_width
    result_left = left + (frame_width - result_width) / 2
    result_bottom = bottom + (frame_height - result_height) / 2
    result_top = result_bottom + result_height
    ax_result = fig.add_axes((
        result_left / fig_width,
        result_bottom / fig_height,
        result_width / fig_width,
        result_height / fig_height))
    scaled_results = results / np.max(np.abs(results) + 1e-6)
    ax_result.imshow(scaled_results, cmap=pos_neg_cmap, vmin=-1, vmax=1)
    blank(ax_result)

    filename = f"{name}.png"
    plt.savefig(filename, dpi=dpi)


def blank(ax, borderwidth=1, color=border_color):
    ax.spines["top"].set_color(color)
    ax.spines["bottom"].set_color(color)
    ax.spines["right"].set_color(color)
    ax.spines["left"].set_color(color)
    ax.spines["top"].set_linewidth(borderwidth)
    ax.spines["bottom"].set_linewidth(borderwidth)
    ax.spines["right"].set_linewidth(borderwidth)
    ax.spines["left"].set_linewidth(borderwidth)
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)


def add_title(ax, x, y, text):
    ax.text(
        x,
        y,
        text,
        fontsize=text_size,
        color=text_color,
        horizontalalignment="center",
        verticalalignment="center",
    )


if __name__ == "__main__":
    image = np.array([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ])
    kernel = np.array([
        [1, .5, 0],
        [.5, 0, -.5],
        [0, -.5, -1]
    ])
    render(image, kernel)
