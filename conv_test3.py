from PIL import Image
import numpy as np
from lodgepole import image_tools as lit 
from conv_two_d import render

kernel_diag_fwd = np.array([
    [1, .5, 0],
    [.5, 0, -.5],
    [0, -.5, -1],
])
kernel_diag_bak = np.array([
    [0, .5, 1],
    [-.5, 0, .5],
    [-1, -.5, 0],
])
kernel_vert = np.array([
    [1, 0, -1],
    [1, 0, -1],
    [1, 0, -1],
])
kernel_horz = np.array([
    [1, 1, 1],
    [0, 0, 0],
    [-1, -1, -1],
])
kernel_diag_fwd_rect = np.array([
    [.5, 1, .5, 0, -.5],
    [1, .5, 0, -.5, -1],
    [.5, 0, -.5, -1, -.5],
])
kernel_diag_bak_rect = np.array([
    [.5, 1, .5],
    [0, .5, 1],
    [-.5, 0, .5],
    [-1, -.5, 0],
    [-.5, -1, -.5],
])
kernel_vert_rect = np.array([
    [-1, 0, 0, 1],
    [1, 0, 0, -1],
    [1, 0, 0, -1],
    [1, 0, 0, -1],
    [-1, 0, 0, 1],
])
kernel_blur = np.array([
    [.01, .02, .05, .02, .01],
    [.02, .05, .1, .05, .02],
    [.05, .1, .4, .1, .05],
    [.02, .05, .1, .05, .02],
    [.01, .02, .05, .02, .01],
])
kernel_sharpen = np.array([
    [-.01, -.02, -.05, -.02, -.01],
    [-.02, -.05, -.1, -.05, -.02],
    [-.05, -.1, 2, .1, -.05],
    [-.02, -.05, -.1, -.05, -.02],
    [-.01, -.02, -.05, -.02, -.01],
])

color_img = np.asarray(Image.open("face.png"))
image = lit.rgb2gray(color_img) / 255

render(image, kernel_diag_fwd, name="face_fwd")
render(image, kernel_diag_bak, name="face_bak")
render(image, kernel_vert, name="face_vert")
render(image, kernel_horz, name="face_horz")
render(image, kernel_blur, name="face_blur_large")
render(image, kernel_sharpen, name="face_sharpen_large")
